# Начинаем с базового образа Alpine
FROM alpine:latest as builder

# Устанавливаем OpenJDK и Apache Maven
RUN apk add --no-cache openjdk8 maven

# Копируем исходный код в контейнер
COPY . /home/nix0n/dvja
WORKDIR /home/nix0n/dvja

# Собираем проект
RUN mvn clean package

# Используем базовый образ Alpine для развертывания приложения
FROM alpine:latest

# Устанавливаем OpenJDK и Tomcat
RUN apk add --no-cache openjdk8 wget tar
RUN wget http://archive.apache.org/dist/tomcat/tomcat-9/v9.0.50/bin/apache-tomcat-9.0.50.tar.gz && \
    tar -xf apache-tomcat-9.0.50.tar.gz && \
    rm apache-tomcat-9.0.50.tar.gz && \
    mv apache-tomcat-9.0.50 /usr/local/tomcat

# Копируем собранный war-файл в контейнер Tomcat
COPY --from=builder /home/nix0n/dvja/target/dvja-1.0-SNAPSHOT.war /usr/local/tomcat/webapps/ROOT.war

# Открываем порт 8080
EXPOSE 8080

# Запускаем Tomcat при старте контейнера
CMD ["/usr/local/tomcat/bin/catalina.sh", "run"]



